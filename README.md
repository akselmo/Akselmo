<img src="https://www.akselmo.dev/assets/images/oc/HuntraLinux.png" alt="My character Huntra made by Saikkunen" width="35%" align="right"/>

# Hi, I'm Akseli! 
During the day I work on KDE development, and as my hobby I make games and other funky projects.

I like to contribute to open source projects, whether it's bug reports or code!

## Notable projects:

- [Artificial-Rage](https://codeberg.org/akselmo/Artificial-Rage)
  - Simple FPS game I'm making with C, using Raylib library. Work in progress!
- [ESO Linux Addon Manager](https://codeberg.org/akselmo/ESOLinuxAddonManager)
  - Addon "manager" for Elder Scrolls Online. Currently just downloads addons from list of URL's.
  - Current iteration made with Python and GTK.
- [Revontuli](https://codeberg.org/akselmo/Revontuli)
  - A color scheme that is shared between multiple editors, tools, etc..
  - Also for KDE Plasma desktop!

## Links to my things

- <a rel="me" href="https://scalie.zone/@aks">https://scalie.zone/@aks</a>
- https://akselmo.itch.io/
- https://ko-fi.com/akselmo
- https://liberapay.com/akselmo/
- https://matrix.to/#/#aksdev-space:matrix.akselmo.dev
